# Web 3
Pra entender melhor a Web 3, vamos voltar e relembrar o que eram as versões anteriores.

#### Web 1.0: O início
- A internet como ela surgiu e se popularizou
- Sites de conteúdo estático e pouca interatividade dos visitantes
- A era dos e-mail e dos mecanismos de busca
- Principais projetos: Yahoo, Alta Vista, Cadê, BOL e Google

#### Web 2.0: A era social
- Considerada a web participativa
- Explosão de blogs e chats, mídias e redes sociais e do conteúdo produzido pelos próprios usuários
- Sites criados já não são estáticos e possuem um layout claramente focado no consumidor 
- Conceitos de criação e otimização de sites já são altamente essenciais
- Todos passaram a ter voz, por meio de ferramentas como YouTube, Messenger, Orkut, Facebook e Wikipédia

#### Web 3: A descentralização
- Hoje em dia toda informação disponível está organizada de forma que os humanos entendam, mas principalmente as máquinas, para elas nos ajudarem a resolver problemas mais complexos. 
- Alguns pensam que a Web 3 será focada em celulares e smartphones, mas o conceito é muito mais amplo e vai de encontro ao mercado de criptomoedas, também podemos pensar em Metaverso, Blockchain Games e Social Media dentro da blockchain, que dentre seus conceitos, possui o take rate onde todo o valor gerado pelos usuários destas plataformas, pode e deve receber muito mais através de soluções descentralizadas.
- Hoje você não tem controle sobre seus dados e muito menos sabe como eles são armazenados. As empresas simplesmente rastreiam e salvam suas informações sem nenhum consentimento, e todos esses dados são de propriedade e controlados por essas empresas, tanto que isso já rendem bons processos para o Facebook.
- Imagine se amanhã o Uber, o iFood ou o Amazon resolvem desligar os servidores que mantém suas aplicações online, quanto isso afetaria a experiência do usuário ou a renda de um trabalhador?
- A liberdade de expressão também está em risco, os governos podem confiscar suas contas bancárias, se acreditarem que você está expressando uma opinião que vai contra ao seu interesse próprio.
- A Web 3 vai nos fazer repensar fundamentalmente como arquitetamos e interagimos com as aplicações, qualquer pessoa é capaz de construir e se conectar com diferentes aplicativos sem a aprovação de uma organização central ou fazer transações sem a dependência de bancos. 
- Até identidade do usuário, não será do mesmo jeito, ao invés de digitar login e senha, ou se cadastrar com o Facebook, você vai simplesmente conectar a sua carteira, essa mudança permitirá uma nova onda de modelos de negócios anteriormente inimagináveis, como as DAO (organizações autônomas descentralizadas).
- No evento Ethereum Rio, agora no mês de março, a Web 3 está sendo um dos temas mais abordados. Os palestrantes tem reforçado a importância de construir a infraestrutura necessária para composibilidade de dados, escalabilidade de troca de informações e armazenamento de dados, porque sem esse processo fica inviável a criação de uma experiência de usuário tão significativa, como resultado, serviços centralizados, a exemplo da Metamask e Brave, acabam capturando boa parte dos usuários.
---
- [Web 3: A Nova Internet](https://blog.orbital.company/web-3-a-nova-internet/)
- [A descentralização da Internet](https://livecoins.com.br/o-que-e-web-3-0-descentralizacao-da-internet/)

# Blockchain 
- É a rede onde ficam armazenados os registros de todas transações utilizando criptomoedas, de forma que esse registro seja confiável e imutável, basicamente ela surgiu para que o Bitcoin pudesse existir, mas que hoje vai muito além disso.
- Por ela ser descentralizada e possuir diversas camadas de segurança, invadi-la é uma tarefa extremamente difícil. E quando o sistema identifica alguma possível invasão, ele automaticamente trava em questão de segundos. 
- Se você ouviu falar sobre criptomoedas roubadas, provavelmente foi falha do usuário ou da corretora – não na blockchain.
- Ela armazena as informações de um grupo de transações em blocos, marcando cada bloco com uma hash (um registro de tempo e data), sua origem e seu destino. E a cada 10 minutos é formado um novo bloco de transações, que se liga ao bloco anterior.
- Os blocos criados são dependentes um dos outros e formam uma cadeia de blocos (por isso o nome: Blockchain). Isso torna a tecnologia perfeita para registro de informações que necessitam de confiança, essa confiança é verificada pelos mineradores, que emprestam seu poder computacional para a rede e recebem tokens como recompensa.
- O minerador só pode adicionar uma transação ao bloco se uma maioria  (50% + 1) da rede concordar que aquela transação é legítima e correta, esse é o mecanismo de consenso da blockchain. 
- Com essa tecnologia é possível criar uma série de produtos revolucionários e disruptivos – como é o caso das criptomoedas.
----
- [O que é Blockchain?](https://blog.orbital.company/o-que-e-blockchain/)
- [Guia para implementar Blockchain](https://101blockchains.com/pt/cio-da-blockchain/)
- [Como a Blockchain está mudando a internet](https://101blockchains.com/pt/exemplos-web-3-0/)

# Criptomoedas

- Moeda digital que possui fundamentos e resolve problemas reais
- É protegida por criptografiaque torna quase impossível sua falsificação
- Não são emitidas por nenhum centro de controle, o que as torna, teoricamente, imunes à interferência ou manipulação do governo
- São criadas por meio da mineração e podem ser compradas em corretoras, ou em aplicações descentralizadas (DeFi), e depois enviadas para sua carteira, tornando-as sua propriedade.
----
- [O que são criptomoedas?](https://blog.orbital.company/o-que-sao-criptomoedas/)

# Slides
- [Apresentação](https://slides.com/d/FVcBShU/live)
